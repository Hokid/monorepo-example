module.exports.functionA = function() {
    console.log('Even Better Function A');
}

module.exports.functionB = function() {
    console.log('Better Function B');
}

module.exports.functionC = function() {
    console.log('Function C');
}
