module.exports = () => ({
    registry: "git@gitlab.com:Hokid/monorepo-example.git",
    getTagName: (pkg) => `${pkg.name}_v${pkg.version}`,
});
